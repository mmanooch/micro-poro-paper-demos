# Welcome to [Genet, 2023, Comptes Rendus Mécanique (In Press)]'s demos!

Interactive demos can be found at https://mgenet.gitlabpages.inria.fr/N-DEG-paper-demos/index.html.
