# Welcome to [Genet, 2023, Comptes Rendus Mécanique (In Press)]'s demos!

Demos can be browsed statically but also interactively—to start a session and run the code just click on the rocket icon at the top of a tutorial page and then click on the Binder icon.
